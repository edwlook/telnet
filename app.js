// var telnet = require('telnet');

// telnet.createServer(function (client) {

//   // make unicode characters work properly
//   client.do.transmit_binary();

//   // make the client emit 'window size' events
//   client.do.window_size();

//   // listen for the window size events from the client
//   // client.on('window size', function (e) {
//   //   if (e.command === 'sb') {
//   //     console.log('telnet window resized to %d x %d', e.width, e.height);
//   //   }
//   // });


//   // listen for the actual data from the client
//   client.on('data', function (b) {
//     client.write('Jordon typed: ' + b);
//   });

//   client.write(
//     '**********************************************************************\n' +
//     'Greetings, Jordon Wing. My name is Parry, your personal parrot.\n'
//   );

// }).listen(3000);

var net = require('net')
, TelnetServerProtocolStream = require('sol-telnet');

//
// Teset Server.
//
var net = require("net");
var server = net.createServer(function(sock){

      var ts = new TelnetServerProtocolStream();

  sock.pipe(ts).pipe(sock);

    // Every time you get a NULL LF you get a line.
  ts.on('lineReceived', function(line){
    console.log(line);
    this.send("Jordon typed: " + line + "\n")
  })

    // Resize your telnet window and this should change.
  // ts.on('clientWindowChangedSize', function(width, height) {
  //   console.log("NAWS:", width, height);
  // })

    // Something odd...
    // ts.on("unhandledCommand", function(data) {
    //   console.log(data);
    // })
  ts.send("\u001B[2J");
  ts.send(
    '**********************************************************************\n' +
    'Greetings, Jordon Wing. My name is Parry, your personal parrot.\n'
  );

})
server.listen(5000);
